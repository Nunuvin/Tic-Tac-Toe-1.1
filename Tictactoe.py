# Do not remove the header from this file or other files created from it
# GPL Version 3
# Provided on AS IS basis without any warranty of any kind, either expressed or implied
# Use it at your own risk
# However please give credit to the creator of the original code 
# Made by Vlad C.
# Tic Tac Toe 1.1.1

from __future__ import print_function # allows use of end=" " to avoid change of line when field is printed out
import os
print("Welcome to Tic Tac Toe 1.1", "\n")
print('Best and the worst game which you will ever play!', "\n")
print ("To play enter the number to put X or O", "\n")
print ("Pay attention to whose turn it is in announcements!", "\n")
field = ["0","1","2","3","4","5","6","7","8"]
win = False
count = 0
while win == False: # win condition met or not
	print ("\n", end=" ") # ensures new line
	for i in range(0,9):
		if i==3 or i==6: # check if its end of the row
			print ("\n", field[i], end=" ")
		else:
			print (field[i], end=" ")	
	turncheck= count!=1 and (count ==0 or count%2 ==0) # even or not
	if turncheck== True: # check if it's p1 or p2 turn. Odd is p2; Even is p1. 
		name =0 #															Check and edit the field data by adding X or O
		print ("\n P1 turn") # below actions by p1
		print ("Write # of the cell where you would like to place X INTEGER ONLY", "\n")
		cell0= raw_input()
		cell0=int(cell0)
		if field[cell0]!="X" and field[cell0]!="O":
			field[cell0]="X"
			print ("Move accepted \n")
		elif cell0>=9:
			print ("Number entered is too big!, enter another number \n")
		else:
			print ("Unacceptable move (space occupied), enter another number! \n")	
	else:
		name=1 # below actions by p2
		print ("P2 turn") 
		print ("\n", "Write # of the cell where you would like to place O INTEGER ONLY", "\n")
		cell0=raw_input()
		cell0=int(cell0)
		if field[cell0]!="X" and field[cell0]!="O":
			field[cell0]="O"
			print ("Move accepted \n")
		elif cell0>=9:
			print ("Number entered is too big!, enter another number \n")
		else:
			print ("Unacceptable move (space occupied), enter another number! \n")					
	if field[0]==field[1] ==field[2]: #win condition checks are same for both players
			win=True						 #Check if 3 adjacent cells are same. if yes determine who exactly won
	if field[3]==field[4] ==field[5]: win=True
	if field[6]==field[7] ==field[8]: win=True
	if field[0]==field[3] ==field[6]: win=True
	if field[1]==field[4] ==field[7]: win=True
	if field[2]==field[5] ==field[8]: win=True
	if field[0]==field[4] ==field[8]: win=True
	if field[6]==field[4] ==field[2]: win=True
	if name == 0 and win==True: # who won
		print ("P1 WON! \n")
		break
	elif name ==1 and win == True:
		print ("P2 WON! \n")
		break
	elif count==8 and win == False: #Check if draw conditions are met.
		print ("DRAW! \n")
	count+=1
print ("If you would like to play again please restart the game! \n")